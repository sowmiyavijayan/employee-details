import React from "react";
import "../views/EmployeeDetails.css";
import Pagination from "react-js-pagination";


class EmployeeDetails extends React.Component{
    state = {
        employeeList:[],
        searchValue:'',
        dataToShow : [],
        perPageItemCount : 10,
        currentPage : 1,
    };
    

    componentDidMount() {
        this.fetchData();
    }
    

    fetchData = () =>{
        fetch("https://hub.dummyapis.com/employee?noofRecords=1000&idStarts=1001")
        .then((res) => res.json())
        .then((json) =>{
            if(json.length > 0){
                this.setState({ employeeList: json }, ()=>this.loadContent());
            }
        })
    };

    onChangeInput =(e)=>{
        this.setState({searchValue : e.target.value})
    }


    onSubmit =()=>{
     const filteredData = this.state.employeeList.filter((employee)=>{     
        return employee.email.toLowerCase().includes(this.state.searchValue.toLowerCase()) ||   employee.contactNumber.includes(this.state.searchValue)
     })
        this.setState({dataToShow : filteredData, currentPage : 1 })
    }
    

    loadContent = () =>{
        const {employeeList,currentPage,perPageItemCount} = this.state;
        let tempArr = [...employeeList];
        const startValue =  (currentPage - 1) * perPageItemCount;
        const endValue = startValue + perPageItemCount;
        let filteredArr = tempArr.slice(startValue, endValue)
        this.setState({dataToShow : filteredArr})  
    }


    onChangePageCount = (event) =>{
        this.setState({perPageItemCount : parseInt(event.target.value),currentPage : 1}, ()=>this.loadContent())
    }


    onChangePagination = (pageNumber)=>{
        this.setState({currentPage : pageNumber}, ()=>this.loadContent())
    }



    render() {
        return(
            <div className="container">
                <div className="inputWrapper">
                    <div className="onChangeInput">
                        <input type="text" onChange={this.onChangeInput} placeholder="Search by email/contact number"></input>
                        <button onClick={this.onSubmit}>Submit</button>
                    </div>

                    <div className="onChangePageCount">
                    <input type="number" onChange={this.onChangePageCount} placeholder="Enter per page item count"></input>
                    </div>
                </div>
                {
                    this.state.dataToShow.length > 0 ? (
                        <div className="table-responsive">
                            <table class="table table-striped table-dark">
                                <tbody>
                                    {this.state.dataToShow.map((empDetails) =>{
                                    return(
                                            <tr>
                                                <td>{empDetails.id}</td>
                                                <td>{empDetails.firstName}</td>
                                                <td>{empDetails.lastName}</td>
                                                <td>{empDetails.email}</td>
                                                <td>{empDetails.contactNumber}</td>
                                                <td>{empDetails.age}</td>
                                                <td>{empDetails.dob}</td>
                                                <td>{empDetails.salary}</td>
                                                <td>{empDetails.address}</td>
                                            </tr>
                                    );
                                    })}
                                </tbody>
                            </table>
                        </div>

                    ) : <div>No result found</div>
                }
                
               
                
                {
                    this.state.dataToShow.length > 0 ? (
                        <Pagination
                            activePage={this.state.currentPage}
                            itemsCountPerPage={this.state.perPageItemCount}
                            totalItemsCount={this.state.employeeList.length}
                            pageRangeDisplayed={5}
                            onChange={this.onChangePagination}
                        /> 

                    ) : " "
                }
                
            </div>
        )
    }
}

export default EmployeeDetails;
